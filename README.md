# Keycloak Sync

## Description

This is a simple service to synchronize sshPublicKey attributes from Keycloak IDP server to a self hosted Gitlab instance, as this feature is disabled on the community version of Gitlab.
We use a K8s CronJob scheduled each 5 minutes to do the synchronization, by connecting to the Keycloak IDP server and Gitlab instance, and fetching the SSH public keys from Keycloak to be
compared with the configured ones at Gitlab for each user. If there are differences, it configures the Gitlab user to match the SSH public keys present in Keycloak.
