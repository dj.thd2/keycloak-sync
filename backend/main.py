from fastapi import Security, Body, FastAPI, Form, Request, HTTPException, Header, Depends
from fastapi.responses import JSONResponse, Response, StreamingResponse
from fastapi.middleware.cors import CORSMiddleware
from fastapi.security.api_key import APIKeyHeader, APIKey
from starlette.status import HTTP_403_FORBIDDEN
from io import BytesIO
from pydantic import BaseModel

import secrets
import json
import time
import os
import re
import logging
import requests
import base64
import asyncio
import urllib.parse

from typing import Type, Union, Optional

from settings import env

from keycloak import KeycloakOpenID
from keycloak import KeycloakAdmin
import gitlab

keycloak_openid = KeycloakOpenID(server_url=env['KEYCLOAK_USER_SERVER_URL'],
                                 realm_name=env['KEYCLOAK_MANAGER_REALM_NAME'],
                                 client_id=env['KEYCLOAK_MANAGER_CLIENT_ID'],
                                 client_secret_key=env['KEYCLOAK_MANAGER_CLIENT_SECRET_KEY'],
)

API_KEY = env['API_KEY']
if API_KEY == '':
    API_KEY = None

API_PREFIX = '/api/v1'

app = FastAPI()

allow_origin_regex = env['ALLOW_ORIGIN_REGEX']

if allow_origin_regex:
    app.add_middleware(
        CORSMiddleware,
        allow_origin_regex=allow_origin_regex,
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )

api_key_header = APIKeyHeader(name="X-Api-Key", auto_error=False)

def get_keycloak_admin():
    keycloak_admin = KeycloakAdmin(server_url=env['KEYCLOAK_MANAGER_SERVER_URL'],
                   #username=env['KEYCLOAK_MANAGER_CLIENT_ID'],
                   #password=env['KEYCLOAK_MANAGER_CLIENT_SECRET_KEY'],
                   client_id=env['KEYCLOAK_MANAGER_CLIENT_ID'],
                   client_secret_key=env['KEYCLOAK_MANAGER_CLIENT_SECRET_KEY'],
                   realm_name=env['KEYCLOAK_MANAGER_REALM_NAME'],
                   user_realm_name=env['KEYCLOAK_USER_REALM_NAME'],
                   verify=True
    )
    return keycloak_admin

def parse_ssh_public_key_string(ssh_public_key: str):
    key_array = ' '.join(ssh_public_key.split()).split(' ')
    return {
        "type": key_array[0] or None,
        "key": key_array[1] or None,
        "name": ' '.join(key_array[2:] or []),
    }


async def verify_api_key(api_key_header: str | None = Security(api_key_header)):
    if API_KEY is not None:
        if not api_key_header:
            logging.error("No API Key header received")
            raise HTTPException(status_code=401, detail="Unauthorized")
        if api_key_header == API_KEY:
            return api_key_header
        else:
            logging.error("Invalid API Key header received")
            raise HTTPException(
                status_code=403, detail="Forbidden"
            )

# Health
@app.get(API_PREFIX + "/health", status_code=200)
def health():
    return Response(content="SUCCESS", media_type="text/plain")

# Generate error 500
@app.get(API_PREFIX + "/generate_error_500", status_code=500)
async def generate_error_500():
    return Response(content="ERROR", media_type="text/plain")

@app.get(API_PREFIX + "/sync_users", status_code=200)
def sync_users(api_key: APIKey = Depends(verify_api_key)):

    keycloak_admin = get_keycloak_admin()
    idp_users = keycloak_admin.get_users({})

    actions = []

    idp_users_dict = {}

    for user in idp_users:
        username = user.get('username', None)
        email = user.get('email', None)
        if username is None or email is None:
            continue
        user_attributes = user.get('attributes', {})
        if not user_attributes:
            continue
        user_ldap_entry_dn = user_attributes.get('LDAP_ENTRY_DN', [])
        if not user_ldap_entry_dn:
            continue
        user_ldap_entry_dn = user_ldap_entry_dn[0]
        user_ldap_id = user_attributes.get('LDAP_ID', [])
        if not user_ldap_id:
            continue
        user_ldap_id = user_ldap_id[0]
        ssh_public_keys = user_attributes.get('sshPublicKey', [])
        idp_users_dict[username] = {
            "username": username,
            "email": email,
            "enabled": user.get("enabled", False),
            "ldap_entry_dn": user_ldap_entry_dn,
            "ldap_id": user_ldap_id,
            "ssh_public_keys": [parse_ssh_public_key_string(ssh_public_key) for ssh_public_key in ssh_public_keys],
        }

    gl = gitlab.Gitlab(url=env['GITLAB_URL'], private_token=env['GITLAB_PRIVATE_TOKEN'])

    gl_users = gl.users.list()

    actions_remove = []
    actions_add = []

    gl_users_dict = {}
    for gl_user in gl_users:
        username = gl_user.username
        gl_user_dict = gl_user.asdict()

        is_idp_synced_user = False
        idp_user = None
        if username in idp_users_dict:
            is_idp_synced_user = True
            idp_user = idp_users_dict[username]

        is_identity_synced_user = False
        for identity in gl_user_dict.get('identities', []):
            provider = identity.get('provider', None)
            extern_uid = identity.get('extern_uid', None)
            if provider not in ['ldapmain', 'openid_connect', 'saml']:
                continue
            is_identity_synced_user = True
            if not idp_user:
                actions_remove.append({"action": "remove_provider", "username": username, "provider": provider, "user": gl_user})
                continue
            match provider:
                case 'ldapmain':
                    ldap_entry_dn = idp_user.get('ldap_entry_dn', None)
                    if extern_uid != ldap_entry_dn:
                        actions_add.append({"action": "update_provider", "username": username, "provider": provider, "current_uid": extern_uid, "new_uid": ldap_entry_dn, "user": gl_user})
                        continue
                case 'openid_connect':
                    idp_username = idp_user.get('username', None)
                    if extern_uid != idp_username:
                        actions_add.append({"action": "update_provider", "username": username, "provider": provider, "current_uid": extern_uid, "new_uid": idp_username, "user": gl_user})
                        continue
                case 'saml':
                    idp_username = idp_user.get('username', None)
                    if extern_uid != idp_username:
                        actions_add.append({"action": "update_provider", "username": username, "provider": provider, "current_uid": extern_uid, "new_uid": idp_username, "user": gl_user})
                        continue

        if not is_identity_synced_user and not is_idp_synced_user:
            continue

        user_state = gl_user_dict.get('state', None)

        if not is_identity_synced_user:
            actions_add.append({"action": "add_identity_provider", "username": username})

        is_user_disabled = False

        if not is_idp_synced_user and user_state == "active":
            actions_remove.append({"action": "block_user", "username": username, "reason": "Does not exist in idp", "user": gl_user})
            is_user_disabled = True

        else:
            if user_state == "active" and not idp_user['enabled']:
                actions_remove.append({"action": "block_user", "username": username, "reason": "Is disabled in idp", "user": gl_user})
                is_user_disabled = True
            elif user_state == "blocked" and idp_user['enabled']:
                actions_add.append({"action": "unblock_user", "username": username, "reason": "Is enabled in idp", "user": gl_user})
                user_state = "active"

        if user_state != "active":
            is_user_disabled = True

        if not idp_user or not idp_user['enabled']:
            is_user_disabled = True

        #keys = []
        keys_list = []
        for key in gl_user.keys.list():
            key_dict = key.asdict()
            key_data = parse_ssh_public_key_string(key_dict.get('key', ''))
            key_data['id'] = key_dict['id']
            keys_list.append(key_data)
            #keys.append(key_dict)

        for key_data in keys_list:
            if is_user_disabled:
                actions_remove.append({"action": "remove_user_key", "username": username, "key": key_data, "reason": "User is not enabled", "user": gl_user})
                continue
            key_synced = False
            for idp_key_data in idp_user['ssh_public_keys']:
                if idp_key_data['type'] == key_data['type'] and idp_key_data['key'] == key_data['key']:
                    key_synced = True
                    break
            if not key_synced:
                actions_remove.append({"action": "remove_user_key", "username": username, "key": key_data, "reason": "Key is not present on idp", "user": gl_user})

        if idp_user:
            for idp_key_data in idp_user['ssh_public_keys']:
                key_synced = False
                for key_data in keys_list:
                    if idp_key_data['type'] == key_data['type'] and idp_key_data['key'] == key_data['key']:
                        key_synced = True
                        break
                if not key_synced:
                    actions_add.append({"action": "add_user_key", "username": username, "key": idp_key_data, "user": gl_user})

        gl_users_dict[username] = {
            "username": username,
            "state": user_state,
            "keys": keys_list,
            #"keys_array": keys_array,
            #"asdict": gl_user_dict,
        }

    #gl_users = [user.asdict() for user in gl.users.list()]

    result = {}
    result["idp_users"] = idp_users_dict
    result["gl_users"] = gl_users_dict

    actions = []

    for action in actions_remove + actions_add:
        actions.append(action)

    for action in actions:
        if action['action'] == 'add_identity_provider':
            continue
        logging.error("Processing action %s - %s ..." % (action['action'], action['username']))
        match action['action']:
            case 'remove_provider':
                action['user'].identityproviders.delete(action['provider'])
            case 'remove_user_key':
                action['user'].keys.delete(action['key']['id'])
            case 'add_user_key':
                action['user'].keys.create({
                    "title": action['key']['name'],
                    "key": action['key']['type'] + ' ' + action['key']['key'],
                })
            case 'block_user':
                action['user'].block()
            case 'unblock_user':
                action['user'].unblock()

    for action in actions:
        if 'user' in action:
            del action['user']

    result['actions'] = actions

    return JSONResponse(result, status_code=200);

