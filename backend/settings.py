import os
from dotenv import load_dotenv

load_dotenv()

env = {
    'LISTEN_HOST': os.getenv('LISTEN_HOST') or '0.0.0.0',
    'PORT': os.getenv('PORT') or '8000',
    'DEBUG': (os.getenv('DEBUG') or 'false') == 'true',
    'API_KEY': os.getenv('API_KEY') or None,
    'ALLOW_ORIGIN_REGEX': os.getenv('ALLOW_ORIGIN_REGEX') or None,
    'KEYCLOAK_MANAGER_SERVER_URL': os.getenv('KEYCLOAK_MANAGER_SERVER_URL') or None,
    'KEYCLOAK_MANAGER_REALM_NAME': os.getenv('KEYCLOAK_MANAGER_REALM_NAME') or None,
    'KEYCLOAK_MANAGER_CLIENT_ID': os.getenv('KEYCLOAK_MANAGER_CLIENT_ID') or None,
    'KEYCLOAK_MANAGER_CLIENT_SECRET_KEY': os.getenv('KEYCLOAK_MANAGER_CLIENT_SECRET_KEY') or None,
    'KEYCLOAK_USER_SERVER_URL': os.getenv('KEYCLOAK_USER_SERVER_URL') or None,
    'KEYCLOAK_USER_REALM_NAME': os.getenv('KEYCLOAK_USER_REALM_NAME') or None,
    'KEYCLOAK_USER_CLIENT_ID': os.getenv('KEYCLOAK_USER_CLIENT_ID') or None,
    'KEYCLOAK_USER_EMAIL_PHONE_FORMAT': os.getenv('KEYCLOAK_USER_EMAIL_PHONE_FORMAT') or '%s@phone',
    'GITLAB_URL': os.getenv('GITLAB_URL') or None,
    'GITLAB_PRIVATE_TOKEN': os.getenv('GITLAB_PRIVATE_TOKEN') or None,
}
