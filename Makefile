init:
	cp .env.example .env
	make down
	make build

build:
	docker-compose build

up:
	docker-compose up --no-build

down:
	docker-compose down --remove-orphans

test:
	make down
	docker-compose run --no-deps --rm --user root backend python3 -m pytest -v

lint:
	make down
	docker-compose run --no-deps --rm --user root backend python3 -m pylint --reports=no --exit-zero --rcfile=pylint.cfg /app
